package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Genre;
import com.biblioteka.bibliotekaapp.Repositories.ClientRepository;
import com.biblioteka.bibliotekaapp.Services.Impl.GenreServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/genre")
public class GenreController {

    @Autowired
    private GenreServiceImpl genreService;

    @DeleteMapping("/{id}")
    public @ResponseBody
    String deletePublisher(@PathVariable("id") Integer id) {
        genreService.delete(genreService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody
    String updatePublisher(@Valid @RequestBody Genre genre) {
        genreService.update(genre);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody
    Iterable<Genre> getAllGenres() {
        return genreService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Genre findGenreById(@PathVariable("id") Integer id) {
        return genreService.find(id);
    }

    @PostMapping("")
    public @ResponseBody
    String create(@Valid @RequestBody Genre genre) {
        genreService.create(genre);
        return "Saved";
    }
}
