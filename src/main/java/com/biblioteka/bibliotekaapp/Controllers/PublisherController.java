package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Person;
import com.biblioteka.bibliotekaapp.Models.Publisher;
import com.biblioteka.bibliotekaapp.Services.Impl.PersonServiceImpl;
import com.biblioteka.bibliotekaapp.Services.Impl.PublisherServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/publishers")
public class PublisherController {

    @Autowired
    private PublisherServiceImpl publisherService;

    @DeleteMapping("/{id}")
    public @ResponseBody
    String deletePublisher(@PathVariable("id") Integer id) {
        publisherService.delete(publisherService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody
    String updatePublisher(@Valid @RequestBody Publisher publisher) {
        publisherService.update(publisher);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody
    Iterable<Publisher> getAllPublishers() {
        return publisherService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Publisher findFublisher(@PathVariable("id") Integer id) {
        return publisherService.find(id);
    }

    @PostMapping("")
    public @ResponseBody
    String create(@Valid @RequestBody Publisher publisher) {
        publisherService.create(publisher);
        return "Saved";
    }
}
