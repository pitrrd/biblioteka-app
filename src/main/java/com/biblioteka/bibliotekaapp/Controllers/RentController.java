package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Rent;
import com.biblioteka.bibliotekaapp.Repositories.ClientRepository;
import com.biblioteka.bibliotekaapp.Services.Impl.RentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/rent")
public class RentController {

    @Autowired
    private RentServiceImpl rentService;

    @DeleteMapping("/{id}")
    public @ResponseBody
    String deleteRents(@PathVariable("id") Integer id) {
        rentService.delete(rentService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody
    String updateRents(@Valid @RequestBody Rent rent) {
        rentService.update(rent);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody
    Iterable<Rent> getAllRents() {
        return rentService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Rent findRents(@PathVariable("id") Integer id) {
        return rentService.find(id);
    }

    @PostMapping("")
    public @ResponseBody
    String create(@Valid @RequestBody Rent rent) {
        rentService.create(rent);
        return "Saved";
    }
}
