package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Client;
import com.biblioteka.bibliotekaapp.Services.Impl.ClientServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientServiceImpl clientService;


    @DeleteMapping("/{id}")
    public @ResponseBody String deleteClient(@PathVariable("id") Integer id) {
        clientService.delete(clientService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody String updateClient(@Valid @RequestBody Client client) {
        clientService.update(client);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody Iterable<Client> getAllClients() {
        return clientService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody Client findClient(@PathVariable("id") Integer id) {
        return clientService.find(id);
    }

    @PostMapping("")
    public @ResponseBody String create (@Valid @RequestBody Client client) {
        clientService.create(client);
        return "Saved";
    }
}
