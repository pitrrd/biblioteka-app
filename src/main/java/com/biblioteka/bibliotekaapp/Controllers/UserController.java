package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Person;
import com.biblioteka.bibliotekaapp.Repositories.ClientRepository;
import com.biblioteka.bibliotekaapp.Services.Impl.ClientServiceImpl;
import com.biblioteka.bibliotekaapp.Services.Impl.EmployeeServiceImpl;
import com.biblioteka.bibliotekaapp.Services.Impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private PersonServiceImpl personService;
    @Autowired
    private ClientServiceImpl clientService;
    @Autowired
    private EmployeeServiceImpl employeeService;


    @DeleteMapping("/{id}")
    public @ResponseBody String deletePerson(@PathVariable("id") Integer id) {
        personService.delete(personService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody String updatePerson(@Valid @RequestBody Person person) {
        personService.update(person);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody Iterable<Person> getAllPersons() {
        return personService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody Person findPerson(@PathVariable("id") Integer id) {
        return personService.find(id);
    }

    @PostMapping("")
    public @ResponseBody String create (@Valid @RequestBody Person person) {
        personService.create(person);
        return "Saved";
    }
}
