package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Book;
import com.biblioteka.bibliotekaapp.Repositories.ClientRepository;
import com.biblioteka.bibliotekaapp.Services.Impl.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookServiceImpl bookService;


    @DeleteMapping("/{id}")
    public @ResponseBody
    String deleteBook(@PathVariable("id") Integer id) {
        bookService.delete(bookService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody
    String updatePerson(@Valid @RequestBody Book book) {
        bookService.update(book);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody
    Iterable<Book> getAllBooks() {
        return bookService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Book findBook(@PathVariable("id") Integer id) {
        return bookService.find(id);
    }

    @PostMapping("")
    public @ResponseBody
    String create(@Valid @RequestBody Book book) {
        bookService.create(book);
        return "Saved";
    }
}