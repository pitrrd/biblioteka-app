package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Employee;
import com.biblioteka.bibliotekaapp.Services.Impl.EmployeeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeServiceImpl employeeService;


    @DeleteMapping("/{id}")
    public @ResponseBody String deleteEmployee(@PathVariable("id") Integer id) {
        employeeService.delete(employeeService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody String updateEmployee(@Valid @RequestBody Employee employee) {
        employeeService.update(employee);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody Iterable<Employee> getAllPersons() {
        return employeeService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody Employee findPerson(@PathVariable("id") Integer id) {
        return employeeService.find(id);
    }

    @PostMapping("")
    public @ResponseBody String create (@Valid @RequestBody Employee employee) {
        employeeService.create(employee);
        return "Saved";
    }
}
