package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Location;
import com.biblioteka.bibliotekaapp.Repositories.ClientRepository;
import com.biblioteka.bibliotekaapp.Services.Impl.LocationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    private LocationServiceImpl locationService;

    @DeleteMapping("/{id}")
    public @ResponseBody
    String deleteLocation(@PathVariable("id") Integer id) {
        locationService.delete(locationService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody
    String updateLocation(@Valid @RequestBody Location location) {
        locationService.update(location);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody
    Iterable<Location> getAllGenres() {
        return locationService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Location findGenreById(@PathVariable("id") Integer id) {
        return locationService.find(id);
    }

    @PostMapping("")
    public @ResponseBody
    String create(@Valid @RequestBody Location location) {
        locationService.create(location);
        return "Saved";
    }
}
