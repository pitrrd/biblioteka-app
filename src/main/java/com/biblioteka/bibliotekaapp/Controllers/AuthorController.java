package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Author;
import com.biblioteka.bibliotekaapp.Models.Book;
import com.biblioteka.bibliotekaapp.Services.Impl.AuthorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/authors")
public class AuthorController {
    @Autowired
    private AuthorServiceImpl authorService;


    @DeleteMapping("/{id}")
    public @ResponseBody
    String deleteAuthor(@PathVariable("id") Integer id) {
        authorService.delete(authorService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody
    String updateAuthor(@Valid @RequestBody Author author) {
        authorService.update(author);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody
    Iterable<Author> getAllAuthors() {
        return authorService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Author findAuthor(@PathVariable("id") Integer id) {
        return authorService.find(id);
    }

    @PostMapping("")
    public @ResponseBody
    String create(@Valid @RequestBody Author author) {
        authorService.create(author);
        return "Saved";
    }
}