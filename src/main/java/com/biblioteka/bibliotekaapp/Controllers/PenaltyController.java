package com.biblioteka.bibliotekaapp.Controllers;

import com.biblioteka.bibliotekaapp.Models.Penalty;
import com.biblioteka.bibliotekaapp.Models.Person;
import com.biblioteka.bibliotekaapp.Repositories.ClientRepository;
import com.biblioteka.bibliotekaapp.Services.Impl.ClientServiceImpl;
import com.biblioteka.bibliotekaapp.Services.Impl.EmployeeServiceImpl;
import com.biblioteka.bibliotekaapp.Services.Impl.PenaltyServiceImpl;
import com.biblioteka.bibliotekaapp.Services.Impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/penalty")
public class PenaltyController {

    @Autowired
    private PenaltyServiceImpl penaltyService;

    @DeleteMapping("/{id}")
    public @ResponseBody String deletePerson(@PathVariable("id") Integer id) {
        penaltyService.delete(penaltyService.find(id));
        return "Deleted";
    }

    @PutMapping("")
    public @ResponseBody String updatePerson(@Valid @RequestBody Penalty penalty) {
        penaltyService.update(penalty);
        return "Updated";
    }

    @GetMapping("")
    public @ResponseBody Iterable<Penalty> getAllPenalty() {
        return penaltyService.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody Penalty findPenalty(@PathVariable("id") Integer id) {
        return penaltyService.find(id);
    }

    @PostMapping("")
    public @ResponseBody String create (@Valid @RequestBody Penalty penalty) {
        penaltyService.create(penalty);
        return "Saved";
    }
}
