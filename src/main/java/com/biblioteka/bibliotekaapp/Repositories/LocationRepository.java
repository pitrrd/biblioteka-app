package com.biblioteka.bibliotekaapp.Repositories;

import com.biblioteka.bibliotekaapp.Models.Location;
import com.biblioteka.bibliotekaapp.Models.Person;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface LocationRepository extends CrudRepository<Location, Integer> {

}
