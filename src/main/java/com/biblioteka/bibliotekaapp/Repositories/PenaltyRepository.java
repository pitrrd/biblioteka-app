package com.biblioteka.bibliotekaapp.Repositories;

import com.biblioteka.bibliotekaapp.Models.Client;
import com.biblioteka.bibliotekaapp.Models.Penalty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface PenaltyRepository extends CrudRepository<Penalty, Integer> {
}
