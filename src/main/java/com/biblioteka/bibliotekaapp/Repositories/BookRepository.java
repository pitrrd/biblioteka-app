package com.biblioteka.bibliotekaapp.Repositories;

import com.biblioteka.bibliotekaapp.Models.Book;
import com.biblioteka.bibliotekaapp.Models.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> {
}
