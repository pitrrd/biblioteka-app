package com.biblioteka.bibliotekaapp.Repositories;

import com.biblioteka.bibliotekaapp.Models.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Integer> {
}
