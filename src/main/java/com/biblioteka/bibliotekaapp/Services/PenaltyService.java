package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Penalty;

public interface PenaltyService {
    void create(Penalty penalty);
    void update(Penalty penalty);
    void delete(Penalty penalty);
    Penalty find(Integer id);
    Iterable<Penalty> findAll();
}
