package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Person;

public interface PersonService {
    void create(Person person);
    void update(Person person);
    void delete(Person person);
    Person find(Integer id);
    Iterable<Person> findAll();
}
