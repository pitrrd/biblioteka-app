package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Rent;

public interface RentService {
    void create(Rent rent);
    void update(Rent rent);
    void delete(Rent rent);
    Rent find(Integer id);
    Iterable<Rent> findAll();
}
