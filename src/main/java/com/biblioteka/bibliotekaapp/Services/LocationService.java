package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Location;

public interface LocationService {
    void create(Location location);
    void update(Location location);
    void delete(Location location);
    Location find(Integer id);
    Iterable<Location> findAll();
}
