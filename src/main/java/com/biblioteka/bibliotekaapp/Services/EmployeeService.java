package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Employee;

public interface EmployeeService {
    void create(Employee employee);
    void update(Employee employee);
    void delete(Employee employee);
    Employee find(Integer id);
    Iterable<Employee> findAll();
}
