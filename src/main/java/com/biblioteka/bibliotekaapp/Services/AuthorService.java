package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Author;

public interface AuthorService {
    void create(Author author);
    void update(Author author);
    void delete(Author author);
    Author find(Integer id);
    Iterable<Author> findAll();
}
