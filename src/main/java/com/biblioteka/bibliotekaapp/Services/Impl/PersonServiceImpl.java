package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Person;
import com.biblioteka.bibliotekaapp.Repositories.PersonRepository;
import com.biblioteka.bibliotekaapp.Services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public void create(Person person) {
        personRepository.save(person);
    }

    @Override
    public void update(Person person) {
       // Person personFromDb = personRepository.findById(person.getId_person()).get();
        //personFromDb = person;
        personRepository.save(person);
    }

    @Override
    public void delete(Person person) {
        personRepository.delete(person);
    }

    @Override
    public Person find(Integer id) {
        return personRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Person> findAll() { return personRepository.findAll(); }
}
