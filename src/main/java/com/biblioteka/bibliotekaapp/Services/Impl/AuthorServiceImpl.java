package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Author;
import com.biblioteka.bibliotekaapp.Repositories.AuthorRepository;
import com.biblioteka.bibliotekaapp.Services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public void create(Author author) {
        authorRepository.save(author);
    }

    @Override
    public void update(Author author) {
       // Author AuthorFromDb = AuthorRepository.findById(Author.getId_Author()).get();
        //AuthorFromDb = Author;
        authorRepository.save(author);
    }

    @Override
    public void delete(Author author) {
        authorRepository.delete(author);
    }

    @Override
    public Author find(Integer id) {
        return authorRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Author> findAll() { return authorRepository.findAll(); }
}
