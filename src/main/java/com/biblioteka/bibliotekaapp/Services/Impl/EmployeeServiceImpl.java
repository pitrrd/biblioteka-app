package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Employee;
import com.biblioteka.bibliotekaapp.Repositories.EmployeeRepository;
import com.biblioteka.bibliotekaapp.Services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void create(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public void update(Employee employee) {
       // Employee employeeFromDb = employeeRepository.findById(employee.getId_employee()).get();
        //employeeFromDb = employee;
        ;
        employeeRepository.save(employee);
    }

    @Override
    public void delete(Employee employee) {
        employeeRepository.delete(employee);
    }

    @Override
    public Employee find(Integer id) {
        return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Employee> findAll() { return employeeRepository.findAll(); }
}
