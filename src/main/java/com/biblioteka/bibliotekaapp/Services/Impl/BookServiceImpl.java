package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Book;
import com.biblioteka.bibliotekaapp.Repositories.BookRepository;
import com.biblioteka.bibliotekaapp.Services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void create(Book book) {
        bookRepository.save(book);
    }

    @Override
    public void update(Book book) {
       // Book BookFromDb = BookRepository.findById(Book.getId_Book()).get();
        //BookFromDb = Book;
        bookRepository.save(book);
    }

    @Override
    public void delete(Book book) {
        bookRepository.delete(book);
    }

    @Override
    public Book find(Integer id) {
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Book> findAll() { return bookRepository.findAll(); }
}
