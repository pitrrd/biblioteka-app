package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Location;
import com.biblioteka.bibliotekaapp.Repositories.LocationRepository;
import com.biblioteka.bibliotekaapp.Services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public void create(Location location) {
        locationRepository.save(location);
    }

    @Override
    public void update(Location location) {
       // Location locationFromDb = locationRepository.findById(location.getId_location()).get();
        //locationFromDb = location;
        locationRepository.save(location);
    }

    @Override
    public void delete(Location location) {
        locationRepository.delete(location);
    }

    @Override
    public Location find(Integer id) {
        return locationRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Location> findAll() { return locationRepository.findAll(); }
}
