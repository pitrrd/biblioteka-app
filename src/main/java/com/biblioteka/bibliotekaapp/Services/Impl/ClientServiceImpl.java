package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Client;
import com.biblioteka.bibliotekaapp.Repositories.ClientRepository;
import com.biblioteka.bibliotekaapp.Services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public void create(Client client) {
        clientRepository.save(client);
    }

    @Override
    public void update(Client client) {
       // Client clientFromDb = clientRepository.findById(client.getId_client()).get();
        //clientFromDb = client;
        clientRepository.save(client);
    }

    @Override
    public void delete(Client client) {
        clientRepository.delete(client);
    }

    @Override
    public Client find(Integer id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Client> findAll() { return clientRepository.findAll(); }
}
