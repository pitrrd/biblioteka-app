package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Rent;
import com.biblioteka.bibliotekaapp.Repositories.RentRepository;
import com.biblioteka.bibliotekaapp.Services.RentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RentServiceImpl implements RentService {

    @Autowired
    private RentRepository rentRepository;

    @Override
    public void create(Rent rent) {
        rentRepository.save(rent);
    }

    @Override
    public void update(Rent rent) {
       // Rent rentFromDb = rentRepository.findById(rent.getId_rent()).get();
        //rentFromDb = rent;
        rentRepository.save(rent);
    }

    @Override
    public void delete(Rent rent) {
        rentRepository.delete(rent);
    }

    @Override
    public Rent find(Integer id) {
        return rentRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Rent> findAll() { return rentRepository.findAll(); }
}
