package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Penalty;
import com.biblioteka.bibliotekaapp.Repositories.PenaltyRepository;
import com.biblioteka.bibliotekaapp.Services.PenaltyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PenaltyServiceImpl implements PenaltyService {

    @Autowired
    private PenaltyRepository penaltyRepository;

    @Override
    public void create(Penalty penalty) {
        penaltyRepository.save(penalty);
    }

    @Override
    public void update(Penalty penalty) {
       // Penalty penaltyFromDb = penaltyRepository.findById(penalty.getId_penalty()).get();
        //penaltyFromDb = penalty;
        penaltyRepository.save(penalty);
    }

    @Override
    public void delete(Penalty penalty) {
        penaltyRepository.delete(penalty);
    }

    @Override
    public Penalty find(Integer id) {
        return penaltyRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Penalty> findAll() { return penaltyRepository.findAll(); }
}
