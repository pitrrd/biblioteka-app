package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Publisher;
import com.biblioteka.bibliotekaapp.Repositories.PublisherRepository;
import com.biblioteka.bibliotekaapp.Services.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherServiceImpl implements PublisherService {

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    public void create(Publisher publisher) {
        publisherRepository.save(publisher);
    }

    @Override
    public void update(Publisher publisher) {
       // Publisher publisherFromDb = publisherRepository.findById(publisher.getId_publisher()).get();
        //publisherFromDb = publisher;
        publisherRepository.save(publisher);
    }

    @Override
    public void delete(Publisher publisher) {
        publisherRepository.delete(publisher);
    }

    @Override
    public Publisher find(Integer id) {
        return publisherRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Publisher> findAll() { return publisherRepository.findAll(); }
}
