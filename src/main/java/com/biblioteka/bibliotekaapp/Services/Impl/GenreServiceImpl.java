package com.biblioteka.bibliotekaapp.Services.Impl;

import com.biblioteka.bibliotekaapp.Models.Genre;
import com.biblioteka.bibliotekaapp.Repositories.GenreRepository;
import com.biblioteka.bibliotekaapp.Services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Override
    public void create(Genre genre) {
        genreRepository.save(genre);
    }

    @Override
    public void update(Genre genre) {
       // Genre genreFromDb = genreRepository.findById(genre.getId_genre()).get();
        //genreFromDb = genre;
        genreRepository.save(genre);
    }

    @Override
    public void delete(Genre genre) {
        genreRepository.delete(genre);
    }

    @Override
    public Genre find(Integer id) {
        return genreRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Genre> findAll() { return genreRepository.findAll(); }
}
