package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Genre;

public interface GenreService {
    void create(Genre genre);
    void update(Genre genre);
    void delete(Genre genre);
    Genre find(Integer id);
    Iterable<Genre> findAll();
}
