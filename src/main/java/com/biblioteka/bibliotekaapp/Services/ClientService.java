package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Client;

public interface ClientService {
    void create(Client client);
    void update(Client client);
    void delete(Client client);
    Client find(Integer id);
    Iterable<Client> findAll();
}
