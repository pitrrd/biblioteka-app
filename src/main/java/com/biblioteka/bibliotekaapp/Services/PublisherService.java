package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Publisher;

public interface PublisherService {
    void create(Publisher publisher);
    void update(Publisher publisher);
    void delete(Publisher publisher);
    Publisher find(Integer id);
    Iterable<Publisher> findAll();
}
