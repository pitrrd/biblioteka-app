package com.biblioteka.bibliotekaapp.Services;

import com.biblioteka.bibliotekaapp.Models.Book;

public interface BookService {
    void create(Book book);
    void update(Book book);
    void delete(Book book);
    Book find(Integer id);
    Iterable<Book> findAll();
}
