package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name="RENT")
public class Rent {
    private @Id @GeneratedValue(strategy= GenerationType.AUTO) Integer id_rent;

    private @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name="id_employee") Employee employee;
    private @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name="id_client") Client client;
    private @OneToMany(cascade=CascadeType.ALL) List<Book> books;
    private @Column(nullable=false)Date rent_date;
    private @Column(nullable=false)Date return_date;

    private @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name="id_penalty") Penalty penalty;
}
