package com.biblioteka.bibliotekaapp.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="AUTHOR")
public class Author {
    private @Id
    @GeneratedValue(strategy= GenerationType.AUTO) Integer id_author;
    private @Column(nullable=false)String name;
    private @Column(nullable=false)String surname;


}
