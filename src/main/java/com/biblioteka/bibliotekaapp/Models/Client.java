package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name="CLIENT")
public class Client {
    private @Id @GeneratedValue(strategy= GenerationType.AUTO) Integer id_client;
    private @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name="id_person") Person person;
    public Client(Person person) {
        this.person = person;
    }
}
