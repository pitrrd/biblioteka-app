package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="PENALTY")
public class Penalty {
    private @Id @GeneratedValue(strategy= GenerationType.AUTO) Integer id_penalty;
    private @Column(nullable=false) Integer amount;
    private @Column(nullable=false) boolean paid;
    private @Column(nullable=false) Date deadline;
}
