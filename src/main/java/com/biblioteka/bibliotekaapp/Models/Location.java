package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name="LOCATION")
public class Location {
    private @Id
    @GeneratedValue(strategy= GenerationType.AUTO) Integer id_location;

    private @Column(nullable=false)int level;
    private @OneToOne(fetch = FetchType.LAZY) Genre genre;
    private @Column(nullable=false)int row;
    private @Column(nullable=false)int shelf;
}
