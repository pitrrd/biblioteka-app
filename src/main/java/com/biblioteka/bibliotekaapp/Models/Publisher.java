package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="PUBLISHER")
public class Publisher {
    private @Id
    @GeneratedValue(strategy= GenerationType.AUTO) Integer id_publisher;
    private @Column(nullable=false)String name;
}
