package com.biblioteka.bibliotekaapp.Models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="GENRE")
public class Genre {
    private @Id @GeneratedValue(strategy= GenerationType.AUTO) Integer id_genre;
    private @Column(nullable=false)String genre;
}
