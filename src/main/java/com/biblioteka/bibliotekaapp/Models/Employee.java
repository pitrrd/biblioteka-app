package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "EMPLOYEE")
public class Employee {
    private @Id
    @GeneratedValue(strategy= GenerationType.AUTO) Integer id_employee;
    private @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name="id_person") Person person;
    private @Column(nullable=false)Date hire_date;
    private @Column(nullable=false)int salary;
    private @Column(nullable=false)int bonus;

}
