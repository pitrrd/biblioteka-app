package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="BOOK")
public class Book {
    private @Id @GeneratedValue(strategy= GenerationType.AUTO) Integer id_book;
    private @Column(nullable=false)String title;
    private @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name="id_publisher") Publisher publisher;
    private @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name="id_location") Location location;

    private @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
            @JoinTable(name = "BOOK_AUTHOR", joinColumns = @JoinColumn(name = "ID_BOOK"), inverseJoinColumns = @JoinColumn(name = "ID_AUTHOR"))
            List<Author> author;
    private @OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
            @JoinTable(name = "BOOK_GENRE", joinColumns = @JoinColumn(name = "ID_BOOK"), inverseJoinColumns = @JoinColumn(name = "ID_GENRE"))
            List<Genre> genre;

}
