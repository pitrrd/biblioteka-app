package com.biblioteka.bibliotekaapp.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name="PERSON")
public class Person {
    private @Id @GeneratedValue(strategy= GenerationType.AUTO) Integer id_person;
    private @Column(nullable=false)String name;
    private @Column(nullable=false)String surname;
    private @Column(nullable=false)Date birth_date;
    private @Column(nullable=false)String pesel;
    private @Column(nullable=false)String email;
    private @Column(nullable=false)String street;
    private @Column(nullable=false)String city;
    private @Column(nullable=false)Integer nr;
    private @Column(nullable=false)String post_code;

    public Person(String name, String surname, Date birth_date, String pesel, String email, String street, String city, Integer nr, String post_code) {
        this.name = name;
        this.surname = surname;
        this.birth_date = birth_date;
        this.pesel = pesel;
        this.email = email;
        this.street = street;
        this.city = city;
        this.nr = nr;
        this.post_code = post_code;
    }
}
